from vectores import diferencia, norma, prod_vectorial

def area_triangulo(Ax, Ay, Az, Bx, By, Bz, Cx, Cy, Cz):
    """Calcula una bella area de triangulo"""
    vect1_X , vect1_Y , vect1_Z = diferencia(Bx, By, Bz, Ax, Ay, Az)
    vect2_X , vect2_Y , vect2_Z = diferencia(Cx, Cy, Cz, Ax, Ay, Az)
    prod_vect_X , prod_vect_Y , prod_vect_Z = prod_vectorial(vect1_X , vect1_Y , vect1_Z, vect2_X , vect2_Y , vect2_Z)
    norma_vectores = norma(prod_vect_X , prod_vect_Y , prod_vect_Z)
    return (norma_vectores / 2)


assert area_triangulo(0, 0, 0, 0, 0, 0, 0, 0, 0) == 0
